package com.ehealth.beans;

import java.util.Date;

import org.springframework.data.annotation.Id;

/**
 * Entity mapped to User table in Mongo db
 * @author Vidya
 *
 */
public class User {

    @Id
    private String id;
    private String email;

    private String firstName;
    private String lastName;
    private String password;
    private String gender;
    private Date dateOfBirth;
    private String userToken;

    public User() {}

    

    public String getId() {
		return id;
	}



	public void setId(String id) {
		this.id = id;
	}



	public String getEmail() {
		return email;
	}



	public void setEmail(String email) {
		this.email = email;
	}



	public String getFirstName() {
		return firstName;
	}



	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}



	public String getLastName() {
		return lastName;
	}



	public void setLastName(String lastName) {
		this.lastName = lastName;
	}



	public String getPassword() {
		return password;
	}



	public void setPassword(String password) {
		this.password = password;
	}



	public String getGender() {
		return gender;
	}



	public void setGender(String gender) {
		this.gender = gender;
	}



	public Date getDateOfBirth() {
		return dateOfBirth;
	}



	public void setDateOfBirth(Date dateOfBirth) {
		this.dateOfBirth = dateOfBirth;
	}

	

	public String getUserToken() {
		return userToken;
	}



	public void setUserToken(String userToken) {
		this.userToken = userToken;
	}



	@Override
    public String toString() {
        return String.format(
                "User[id=%s, firstName='%s', lastName='%s']",
                id, firstName, lastName);
    }

}

