package com.ehealth.controllers;

import javax.servlet.http.HttpServletRequest;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

import com.ehealth.dto.ServiceResponseDto;
/**
 * UserController handles all the requests related to User
 * @author Vidya
 *
 */
@Controller

public class UserController {

	/**
	 * loadSignupPage loads the SignupUser.htm page
	 * @param view
	 * @param model
	 * @return
	 */
	@RequestMapping(value = "/signup", method = RequestMethod.GET)
	public String loadSignupPage(ModelAndView view, Model model) {
		System.out.println("REQUEST FOR SIGNUP PAGE SENT");
		model.addAttribute("contentUrl","SignupUser.htm"); 
		return "template"; 
	}

	
	/**
	 * loadSignupPage loads the SignupUser.htm page
	 * @param view
	 * @param model
	 * @return
	 */
	@RequestMapping(value="/activate", method = RequestMethod.GET)
	public String loadActivationPage(HttpServletRequest request, Model model) {
		System.out.println("REQUEST FOR Acivate PAGE SENT");
		String token = request.getParameter("code");
		if(token == null) token = "";
		model.addAttribute("contentUrl","Activate.jsp");
		model.addAttribute("userToken",token);		
		return "template"; 
	}
}
