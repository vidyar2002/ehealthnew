package com.ehealth.controllers;

import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

public class LoginController {

	/**
	 * loadIndexPage loads the index.htm page
	 * @param view
	 * @param model
	 * @return 
	 */
	@RequestMapping(value="/login", method = RequestMethod.GET)
	public String loadLoginPage(ModelAndView view, Model model) {
		System.out.println("REQUEST FOR Login PAGE SENT");
		model.addAttribute("contentUrl","UserLogin.htm");
		return "template"; 
	}
}
