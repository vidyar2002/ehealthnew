package com.ehealth.util;

import java.io.IOException;
import java.util.Date;
import java.util.Properties;
import java.util.ResourceBundle;

import javax.mail.Message;
import javax.mail.PasswordAuthentication;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.PropertySource;
import org.springframework.core.env.Environment;
import org.springframework.core.io.support.PropertiesLoaderUtils;
import org.springframework.stereotype.Component;

/**
 * Util class to send activation email to users
 * 
 * @author Vidya
 *
 */
@Component
@PropertySource("/WEB-INF/classes/mail.properties")
public class EmailUtility {

	@Autowired
	private Environment env;

	@Autowired
	Properties props;

	/**
	 * Sends activation email to the given email address with the token
	 * 
	 * @param toEmailAddress
	 * @param token
	 */
	public void sendEmail(String toEmailAddress, String token) throws Exception {

		System.out.println("From property source : "
				+ env.getProperty("mail.smtp.host"));

		props.put("mail.smtp.host", env.getProperty("mail.smtp.host")); 
		props.put("mail.smtp.auth", env.getProperty("mail.smtp.auth"));
		props.put("mail.debug", env.getProperty("mail.debug"));
		props.put("mail.smtp.starttls.enable",
				env.getProperty("mail.smtp.starttls.enable"));
		props.put("mail.smtp.port", env.getProperty("mail.smtp.port"));
		props.put("mail.smtp.socketFactory.port",
				env.getProperty("mail.smtp.socketFactory.port"));
		props.put("mail.smtp.socketFactory.class",
				env.getProperty("mail.smtp.socketFactory.class"));
		props.put("mail.smtp.socketFactory.fallback",
				env.getProperty("mail.smtp.socketFactory.fallback"));

		Session mailSession = Session.getInstance(props,
				new javax.mail.Authenticator() {

					protected PasswordAuthentication getPasswordAuthentication() {
						return new PasswordAuthentication(env
								.getProperty("authenticationEmail"), env
								.getProperty("authenticationPassword"));
					}
				});

		mailSession.setDebug(true); // Enable the debug mode

		Message msg = new MimeMessage(mailSession);
		msg.setFrom(new InternetAddress(env.getProperty("fromEmail")));
		msg.setRecipients(Message.RecipientType.TO,
				InternetAddress.parse(toEmailAddress));
		msg.setSentDate(new Date());
		msg.setSubject(env.getProperty("mailSubject"));
		msg.setContent(
				generateMailContent(toEmailAddress, token,
						env.getProperty("mailUrl")), "text/html");
		Transport.send(msg);

	}

	/**
	 * Generates mail content for user account activation
	 * 
	 * @param email
	 * @param token
	 * @param url
	 * @return
	 */

	private String generateMailContent(String email, String token, String url) {

		StringBuffer str = new StringBuffer();
		str.append("Hello " + email + ", <br><br>");

		str.append("<p>Your myHealth account has been created, but your ");
		str.append("email address needs to be verified before your account ");
		str.append("becomes fully active. Please click the link below to activate your account within 30 days: </p><br><br>");

		str.append("<a href='" + url + "/activate?code=" + token
				+ "'>Click here to activate your account </a><br><br>");

		str.append("If the link above does not work, paste this into your browser: <br>");
		str.append("<a href='" + url + "/activate?code=" + token + "'>" + url
				+ "/activate?code=" + token + "</a><br>");

		str.append("By clicking the activation link above, you are indicating you have read and agree to the Zoom Terms of Service.<br>");

		str.append("If you need additional help, please visit our Support Center.<br><br>");

		str.append("Thank you.<br>");
		str.append("myHealth Team");

		return str.toString();

	}

}