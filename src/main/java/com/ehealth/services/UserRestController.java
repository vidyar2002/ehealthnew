package com.ehealth.services;

import java.util.Base64;
import java.util.Base64.Encoder;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.ehealth.beans.User;
import com.ehealth.dao.interfaces.UserDao;
import com.ehealth.dao.interfaces.UserRepository;
import com.ehealth.dto.ServiceResponseDto;
import com.ehealth.dto.UserDto;
import com.ehealth.util.EmailUtility;

/**
 * RestController to handle all the rest call related to User mangement
 * 
 * @author Vidya
 *
 */
@RestController
@RequestMapping(value = "/rest/user")
public class UserRestController {

	@Autowired
	private UserRepository repository;
	
	@Autowired
	private UserDao userDao;
	
	
	@Autowired
	private EmailUtility emailUtility;

	/**
	 * checkEmailExists - checks if User exists with the given email id
	 * 
	 * @param request
	 * @param emailAddr
	 * @return ServiceResponseDto with message
	 */
	@RequestMapping(value = "/checkEmailExists", produces = "application/json", consumes = "application/json")
	public ServiceResponseDto checkEmailExists(HttpServletRequest request,
			@RequestBody String emailAddr) {
		ServiceResponseDto response = new ServiceResponseDto();

		System.out.println("inside UserRestController");
		boolean flag = false;
		for (User user : repository.findAll()) {
			if (user.getEmail().equals(emailAddr)) {
				flag = true;
				break;
			}
		}
		System.out.println("Email exists : " + flag);
		if (flag)
			response.setStatusMessage("ERROR : Exists");
		else
			response.setStatusMessage("INFO : Not Exists");

		return response;
	}

	/**
	 * addUser - inserts a User entry to Mongodb
	 * @param request
	 * @param emailAddr
	 * @return
	 */
	@RequestMapping(value = "/addUser", method=RequestMethod.POST, produces = "application/json", consumes = "application/json")
	public ServiceResponseDto addUser(HttpServletRequest request,
			@RequestBody String emailAddr) {
		
		ServiceResponseDto response = new ServiceResponseDto();

		System.out.println("inside addUser method");
		User user = new User();
		user.setEmail(emailAddr);
		Encoder encoder = Base64.getEncoder();
		String token = encoder.encodeToString(emailAddr.getBytes());
		System.out.println("Token = "+token);
		user.setUserToken(token); 
		repository.save(user);		
		try {
			emailUtility.sendEmail(emailAddr, token);
			response.setStatusMessage("Success");
		} catch (Exception e) {
			response.setStatusMessage("ERRROR : "+e.getMessage());
		}

		return response;
	}
	
	/**
	 * getUserInformationByToken - retrievs user information based on token
	 * @param request
	 * @param token
	 * @return ServiceResponseDto
	 */
	@RequestMapping(value = "/getUserInformationByToken", method=RequestMethod.POST, produces = "application/json")
	public ServiceResponseDto getUserInformationByToken(HttpServletRequest request,
			@RequestBody String token) {
		
		ServiceResponseDto response = new ServiceResponseDto();		
		System.out.println("inside getUserInformationByToken method");
		User user = userDao.findByToken(token);
		UserDto dto = new UserDto(user);
		response.setPayload(dto);
		return response;
	}
	

	/**
	 * activateUser - Updates the user profile and activate
	 * @param request
	 * @param UserDto
	 * @return ServiceResponseDto
	 */
	@RequestMapping(value = "/activateUser", method=RequestMethod.POST, produces = "application/json")
	public ServiceResponseDto activateUser(HttpServletRequest request,
			@RequestBody UserDto userDto) {
		
		ServiceResponseDto response = new ServiceResponseDto();		
		System.out.println("inside activateUser method");
		if(userDto != null)
		{
			User user = userDto.generateUser();
			user.setUserToken(null);
			repository.save(user);			
			response.setPayload(userDto);
			response.setStatusMessage("Activated");
		}
		else
		{
			response.setStatusMessage("ERRROR  : User information is not available");
		}
		return response;
	}
}
