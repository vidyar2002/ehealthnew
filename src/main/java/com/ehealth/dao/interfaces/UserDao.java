package com.ehealth.dao.interfaces;

import org.springframework.stereotype.Component;

import com.ehealth.beans.User;

@Component
public interface UserDao {

	/**
	 * findByToken gets the User information based on token
	 * @param token
	 * @return User
	 */
    public User findByToken(String token);
}
