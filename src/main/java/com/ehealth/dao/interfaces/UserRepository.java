package com.ehealth.dao.interfaces;

import java.util.List;

import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.data.mongodb.repository.Query;

import com.ehealth.beans.User;
/**
 * Interface to handle operations with Collection - User
 * @author Vidya
 *
 */
public interface UserRepository extends MongoRepository<User, String> {
	@Query("{ 'userToken' : ?0 }")
    User findByUserToken(String userToken);
}
