package com.ehealth.dao;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.mongodb.core.MongoOperations;
import org.springframework.data.mongodb.core.query.BasicQuery;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.data.mongodb.repository.query.MongoEntityInformation;
import org.springframework.data.mongodb.repository.support.SimpleMongoRepository;
import org.springframework.stereotype.Component;

import com.ehealth.beans.User;
import com.ehealth.dao.interfaces.UserDao;
import com.ehealth.dao.interfaces.UserRepository;
/**
 * 
 * @author Vidya
 *
 */

@Component
public class UserDaoImpl implements UserDao
{

	@Autowired
	UserRepository repository;
	
	/**
	 * findByToken gets the User information based on token
	 * @param token
	 * @return User
	 */
	@Override
	public User findByToken(String token) {
		User user = repository.findByUserToken(token);		
		return user;
	}	
	
}