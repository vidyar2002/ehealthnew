package com.ehealth.dto;

import java.util.Date;

import org.springframework.data.annotation.Id;

import com.ehealth.beans.User;

/**
 * Entity mapped to User table in Mongo db
 * @author Vidya
 *
 */
public class UserDto {

   
    private String id;
    private String email;
    private String firstName;
    private String lastName;
    private String password;
    private String gender;
    private Date dateOfBirth;
    private String userToken;

    public UserDto()
    {
    	
    }
    
    public UserDto(User user) {
    	this.id = user.getId();
    	this.email = user.getEmail();
    	this.firstName = user.getFirstName();
    	this.lastName = user.getLastName();
    	this.password = user.getPassword();
    	this.gender = user.getGender();
    	this.dateOfBirth = user.getDateOfBirth();
    	this.userToken = user.getUserToken();
    }


    public String getId() {
		return id;
	}



	public void setId(String id) {
		this.id = id;
	}



	public String getEmail() {
		return email;
	}



	public void setEmail(String email) {
		this.email = email;
	}



	public String getFirstName() {
		return firstName;
	}



	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}



	public String getLastName() {
		return lastName;
	}



	public void setLastName(String lastName) {
		this.lastName = lastName;
	}



	public String getPassword() {
		return password;
	}



	public void setPassword(String password) {
		this.password = password;
	}



	public String getGender() {
		return gender;
	}



	public void setGender(String gender) {
		this.gender = gender;
	}



	public Date getDateOfBirth() {
		return dateOfBirth;
	}



	public void setDateOfBirth(Date dateOfBirth) {
		this.dateOfBirth = dateOfBirth;
	}

	

	public String getUserToken() {
		return userToken;
	}



	public void setUserToken(String userToken) {
		this.userToken = userToken;
	}

	/**
	 * Generates User entity from DTO
	 * @return
	 */
	public User generateUser()
	{
		User user = new User();
		user.setId(this.id);
		user.setEmail(this.email);
		user.setFirstName(this.firstName);
		user.setLastName(this.lastName);
		user.setGender(this.gender);
		user.setDateOfBirth(this.dateOfBirth);
		user.setUserToken(this.userToken);
		return user;
	}


	@Override
    public String toString() {
        return String.format(
                "User[id=%s, firstName='%s', lastName='%s',email='%s', password='%s', gender='%s']",
                id, firstName, lastName,email, password, gender);
    }

}

