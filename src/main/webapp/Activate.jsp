<script src="js/services/calendarDirective.js"></script>
<!-- ======== @Region: #content ======== -->
<div id="activateUser" ng-controller="ActivationController" ng-init="loadActivateUser()">
	<div class="container">
		<!-- Sign Up form -->
		<div >
			<form class="form-login form-wrapper form-medium" role="form" name="activateForm">
				<p class="alert-danger">{{errorMessage}} </p>
				<h3 class="title-divider">
					<span>Activate your account</span>
				</h3>
				<div class="form-group">
					<h4>Hi {{userDto.email}}</h4>
					<p>Thank you for signing up. Your account has been successfully activated.  Please set your profile details here.</p>
				</div>
				<div class="form-group">
					<label class="sr-only" for="activate-firstName">First Name</label> <input type="text"
						class="form-control" id="firstName" ng-model="userDto.firstName"
						name="firstName" placeholder="First Name" required>
						<span ng-show="activateForm.firstName.$error.required">Please enter your first name.</span>
				</div>
				<div class="form-group">
					<label class="sr-only" for="activate-lastName">Last Name</label> <input type="text"
						class="form-control" id="lastName" ng-model="userDto.lastName"
						name="lastName" placeholder="Last Name" required>
						<span ng-show="activateForm.lastName.$error.required">Please enter your last name.</span>
				</div>
				<div class="form-group">
					<label class="sr-only" for="activate-password">Password</label> <input type="password"
						class="form-control" id="password" ng-model="userDto.password"
						name="password"  placeholder="Password" required>
						<span ng-show="activateForm.password.$error.required">Please enter password.</span>
				</div>
				<div class="form-group">
					<label class="sr-only" for="activate-password">Confirm Password</label> <input type="password"
						class="form-control" id="password" ng-model="confirmPassword"
						name="confirmPassword"  placeholder="Confirm Password" required>
						<span ng-show="activateForm.confirmPassword.$error.required">Please re-enter password to confirm.</span>
				</div>
				<div class="radio">
					<label><input type="radio"
						id="gender" ng-model="userDto.gender"
						name="gender"  required> Male &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
						<input type="radio"
						 id="" ng-model="userDto.gender"
						name="gender"  required> Female
						</label> 
				</div>
				<div class="form-group">
					<label class="sr-only" for="activate-dob">Date of Birth</label> 
						<input type="date" id="test"  name="birthdate" ng-model="userDto.dateOfBirth" popup="dd MMM yyyy" options="dateOptions"
       								 opened="opened" custom-datepicker/>
       								 
							<script type="text/ng-template" id="custom-datepicker.html">
    						<div class="form-inline enhanced-datepicker">
        <label>
            <input
                type="text"
                id="{{id}}"
                name="{{name}}"
                ng-model="ngModel"
                datepicker-popup="{{popup}}"
                datepicker-options="{{options}}"
                date-disabled="{{dateDisabled}}"
                min="{{min}}"
                max="{{max}}"
                open="opened"
                ng-pattern="/^(?:[1-9]|1\d|2\d|3[0-1]) (?:jan|feb|mar|apr|may|jun|jul|aug|sep|oct|nov|dec) (?:1|2)(?:\d{3})$/gim"/>
            <span class="btn"><i class="icon-calendar"></i></span>
        </label>
    </div>
				</script>
				</div>
				<button class="btn btn-primary" type="button" ng-click="activate()">Activate</button>
			</form>
		</div>
		
	</div>

</div>


