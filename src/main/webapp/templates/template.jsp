<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html lang="en"  ng-app="ehealth">
  <head >
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <meta charset="utf-8">
    <title>eHealth Application</title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
     
    <!-- Bootstrap CSS -->
    <link href="css/bootstrap.min.css" rel="stylesheet"> 
    
    <!-- Font Awesome -->
    <link href="css/font-awesome.min.css" rel="stylesheet">
    
    <!-- Plugins required on all pages NOTE: Additional non-required plugins are loaded ondemand as of AppStrap 2.5 -->
    <!-- Plugin: animate.css (animated effects) - http://daneden.github.io/animate.css/ -->
    <link href="plugins/animate/animate.css" rel="stylesheet">
    <!-- @LINEBREAK -- <!-- Plugin: flag icons - http://lipis.github.io/flag-icon-css/ -->
    <link href="plugins/flag-icon-css/css/flag-icon.min.css" rel="stylesheet">
    
    <!-- Theme style -->
    <link href="css/theme-style.min.css" rel="stylesheet">
    
    <!--Your custom colour override-->
    <link href="#" id="colour-scheme" rel="stylesheet">
    
    <!-- Your custom override --> 
    <link href="css/custom-style.css" rel="stylesheet">
    
    <!-- HTML5 shiv & respond.js for IE6-8 support of HTML5 elements & media queries -->
    <!--[if lt IE 9]>
    <script src="plugins/html5shiv/dist/html5shiv.js"></script>
    <script src="plugins/respond/respond.min.js"></script>
    <![endif]-->
    
    <!-- Le fav and touch icons - @todo: fill with your icons or remove -->
    <link rel="shortcut icon" href="img/icons/favicon.png">
    <link rel="apple-touch-icon-precomposed" sizes="114x114" href="img/icons/114x114.png">
    <link rel="apple-touch-icon-precomposed" sizes="72x72" href="img/icons/72x72.png">
    <link rel="apple-touch-icon-precomposed" href="img/icons/default.png">
    <link href='http://fonts.googleapis.com/css?family=Open+Sans:400,700,300' rel='stylesheet' type='text/css'>
    <link href='http://fonts.googleapis.com/css?family=Rambla' rel='stylesheet' type='text/css'>
    <link href='http://fonts.googleapis.com/css?family=Calligraffitti' rel='stylesheet' type='text/css'>
    
        <!--Plugin: Retina.js (high def image replacement) - @see: http://retinajs.com/-->
    <script src="plugins/retina/dist/retina.min.js"></script> 
  
	<!--Scripts -->
	<script src="js/jquery.min.js"></script>
	
	<!--Custom scripts mainly used to trigger libraries/plugins -->
	<script src="js/script.min.js"></script>
	<script src="lib/angular/angular.min.js"></script>
	<script src="lib/angular/angular-resource.js"></script>
	<script src="lib/bootstrap/3.2.0/js/bootstrap.min.js"></script>
	<script src="lib/angular-ui/0.11.2/ui-bootstrap-tpls-0.11.2.min.js"></script>
	<script src="js/app.js"></script>
	<script src="js/services/restServices.js"></script>
	<script src="js/controllers/templateController.js"></script> 
	<script src="js/controllers/signupController.js"></script>  	 
	<script src="js/controllers/activationController.js"></script>

  </head>
  
   <!-- ======== @Region: body ======== -->
  <body class="page page-signup" ng-controller="TemplateController" ng-init="load('${contentUrl}')">  
  <!-- ======== @Region: navigation ======== --> 
	<div id="navigation" class="wrapper" ng-include ="'navigation.htm'"> </div>
<!-- ======== @Region: #content ======== -->
<div id="content">
  <ng-include src="contentUrl"></ng-include>  
  <input type="hidden" id="userToken" name="userToken" value="${userToken}"/>
</div>  

<!-- ======== @Region: #content-below ======== -->
<div id="content-below" class="wrapper">

</div>
<!-- FOOTER --> 

<!-- ======== @Region: #footer ======== -->
<footer id="footer">
 <div ng-include="'footer.htm'"/>
</footer>
</body>
</html>