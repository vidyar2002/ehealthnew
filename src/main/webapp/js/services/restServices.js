ehealth.factory("restServices", function($resource) {
    return $resource("", null,
    {
    	"checkEmailExists":	  {method:"POST", url:"rest/user/checkEmailExists"},
    	"addUser":	  		  {method:"POST", url:"rest/user/addUser"},
    	"getUserInformationByToken" : {method:"POST", url:"rest/user/getUserInformationByToken"},
    	"activateUser" : {method:"POST", url:"rest/user/activateUser"}
    });
});