
ehealth.controller('ActivationController', function($scope, $http,$window,restServices) {
	
	$scope.loadActivateUser = function() {
		var userToken = document.getElementById('userToken').value;		
		restServices.getUserInformationByToken(userToken, function(data) {
			if(!data.error)			
			{
				$scope.userDto = data.payload;				
			}
		});
	};
	
	
	$scope.activate = function()
	{
		restServices.activateUser($scope.userDto, function(data) {
			if(!data.error)
			{
				
				$window.location.href='login';
			}
			else
			{
				$scope.errorMessage = "Unexpected error occured during updating the profile. Please try again"
			}
		});
	}
});	

