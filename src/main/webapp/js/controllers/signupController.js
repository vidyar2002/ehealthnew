var signupCtrl = ehealth.controller('SignupController', function($scope, $http,$modal,restServices) {
	
	$scope.confirmationSection = false;
	
	$scope.signup = function() {
		if($scope.emailAddr == null)
			$scope.duplicateMessage = "Please enter your email id";
		restServices.checkEmailExists($scope.emailAddr, function(data) {
			if(data.error)
			{
				$scope.duplicateMessage = "User already registered with same email id";
			} 
			else
			{
				$scope.duplicateMessage = "";
				$scope.opts = {
						backdrop: true,
						backdropClick: true,
						dialogFade: false,
						keyboard: true,
						templateUrl : 'signupModal',
						controller : ModalInstanceCtrl,
						resolve: {
							email: function () {
					          return $scope.emailAddr;
					        }
						} 
					};
				var modalInstance = $modal.open($scope.opts);
			}
		});
	};
});	

var ModalInstanceCtrl = function ($scope, $modalInstance, email, restServices) {

	  $scope.email = email;
	  
	  $scope.confirmSignUp = function()
	  {
		  restServices.addUser($scope.email, function(data) {
			  
			  $modalInstance.close();
			  angular.element(document.getElementById('signUpContent')).scope().confirmationSection = true;
			  
		  });
	  };
	  
	  $scope.cancel = function()
	  {
		  $modalInstance.close();
	  }
};
	
